//Global THREE
var camera, renderer, scene;

var pauseCamera, pauseScene;

var timeStamp;

var ball;

let directionalLight, spotLight;

let controls;

let paused;

var bbox;

var pauseSceneWidth = 15, pauseSceneHeight = 15;

// veloci maxima
const MAXOMEGA = 0.003;

// aceleracao angular
const ALPHAVALUE = 0.000001;

// para apontar para cima
const AUXVEC = new THREE.Vector3(0, 1, 0);

let meshes = [];

function init() {
  'use strict';
  renderer = new THREE.WebGLRenderer({
    antialias: true
  });
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.autoClear = false;
  document.body.appendChild(renderer.domElement);

  window.addEventListener("resize", onResize);
  window.addEventListener("keydown", onKeyDown);
  window.addEventListener("keyup", onKeyUp);

  createScene();

  meshes.push(new Board(40, 4, 40, 0, 0, 0, 'textures/chessTexture.jpg').mesh);
  meshes.push(new RubicksCube(6, 0, 5, 0, 'textures/rubicksCubeTexture.png', 'textures/bumpMap.png').mesh);
  ball = new PoolBall(3, 10, 5, 10, 'textures/poolBallTexture.jpeg');
  meshes.push(ball.mesh);

  createLights();
  createCamera();
  createPauseScreen();

  controls = new THREE.OrbitControls(camera);

  controls.autoRotate = true;
  controls.autoRotateSpeed = 0.5;

  paused = false;

  timeStamp = Date.now();
}

function createPauseScreen() {
  pauseScene = new THREE.Scene();

  pauseCamera = new THREE.OrthographicCamera(-20, 20, 20, -20, 0, 200);
  pauseCamera.position.x = 30;
  pauseCamera.position.y = 0;
  pauseCamera.position.z = 0;

  pauseCamera.lookAt(pauseScene.position);
  pauseCamera.updateProjectionMatrix();

  const geometry = new THREE.BoxGeometry(10, 10, 32, 1, 1, 1);
  const texture = new THREE.TextureLoader().load("textures/pause.png");
  const material = new THREE.MeshBasicMaterial({
    map: texture,
    transparent: true,
    opacity: 1
  });
  const box = new THREE.Mesh(geometry, material);
  box.position.set(0, 0, 0);
  box.rotateY(Math.PI / 2);
  pauseScene.add(box);



}


function render() {
  'use strict';
  renderer.clear()
  renderer.render(scene, camera);
  renderer.clearDepth();
  if (paused) renderer.render(pauseScene, pauseCamera);
}



function createScene() {
  'use strict';
  scene = new THREE.Scene();
}



function createCamera() {
  'use strict';

  camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 2, 300);

  camera.position.x = 30;
  camera.position.z = 30;
  camera.position.y = 30;

  camera.lookAt(scene.position);
  camera.updateProjectionMatrix();

}



function animate() {
  'use strict';

  let time = Date.now();
  let delta_t = time - timeStamp;
  timeStamp = time;

  if (!paused) {
    ball.calculatePos(delta_t);
    ball.updateVelocity(delta_t);
  }

  render();

  if (!paused) controls.update();

  requestAnimationFrame(animate);
}



function onResize() {
  'use strict';
  renderer.setSize(window.innerWidth, window.innerHeight);
  camera.aspect = renderer.getSize().width / renderer.getSize().height;
  camera.updateProjectionMatrix();

  controls.update();

  let ratio = Math.min(window.innerWidth/ pauseSceneWidth, window.innerHeight / pauseSceneHeight);

  pauseCamera.top = window.innerHeight / ratio;
	pauseCamera.right = window.innerWidth / ratio;

	pauseCamera.bottom = -pauseCamera.top;
	pauseCamera.left = -pauseCamera.right;
	pauseCamera.updateProjectionMatrix();


}


function onKeyDown(e) {
  'use strict';

  switch (e.keyCode) {
    case 82: //R
      if (paused)
        reset();
      break;

    case 66: //B
      if (!paused) {
        if (ball.alpha <= 0)
          ball.alpha = ALPHAVALUE;
        else
          ball.alpha = -ALPHAVALUE;
      }
      break;

    case 87: //W
      switchMaterials();
      break;

    case 76: //L
      switchLights();
      break;

    case 68: //D
      directionalLight.visible = directionalLight.visible ? false : true;
      break;

    case 80: //P
      spotLight.visible = spotLight.visible ? false : true;
      break;

    case 83: //S
      paused = !paused;
      break;
  }
}



function onKeyUp(e) {
  'use strict';

  switch (e.keyCode) {

  }
}

function switchLights() {
  let i;
  for (i = 0; i < meshes.length; i++) {
    meshes[i].changeMaterial();
  }
}

function switchMaterials() {
  let i;
  for (i = 0; i < meshes.length; i++)
    meshes[i].togleWireframes();
}


class Board {

  constructor(width, height, depth, x, y, z, filename) {
    'use strict';
    this.object = new THREE.Object3D();
    this.object.position.set(x, y, z);

    let texture = new THREE.TextureLoader().load(filename);


    this.geometry = new THREE.CubeGeometry(width, height, depth, 35, 35, 35);
    let pmaterials = [
      new THREE.MeshPhongMaterial({
        color: 0x666666,
        specular: 0x222222,
        emissive: 0x000000,
        shininess: 3
      }), new THREE.MeshPhongMaterial({
        color: 0x666666,
        specular: 0x222222,
        emissive: 0x000000,
        shininess: 3
      }), new THREE.MeshPhongMaterial({
        map: texture,
        specular: 0x222222,
        emissive: 0x000000,
        shininess: 3
      }), new THREE.MeshPhongMaterial({
        color: 0x666666,
        specular: 0x222222,
        emissive: 0x000000,
        shininess: 3
      }), new THREE.MeshPhongMaterial({
        color: 0x666666,
        specular: 0x222222,
        emissive: 0x000000,
        shininess: 3
      }), new THREE.MeshPhongMaterial({
        color: 0x666666,
        specular: 0x222222,
        emissive: 0x000000,
        shininess: 3
      })
    ];
    let bmaterials = [new THREE.MeshBasicMaterial({
      color: 0x666666
    }), new THREE.MeshBasicMaterial({
      color: 0x666666
    }), new THREE.MeshBasicMaterial({
      map: texture
    }), new THREE.MeshBasicMaterial({
      color: 0x666666
    }), new THREE.MeshBasicMaterial({
      color: 0x666666
    }), new THREE.MeshBasicMaterial({
      color: 0x666666
    })];
    this.mesh = new SceneMultiMesh(bmaterials, pmaterials, this.geometry);
    this.mesh.position.set(0, 0, 0);
    this.object.add(this.mesh);
    scene.add(this.object);

  }
}



class RubicksCube {

  constructor(width, x, y, z, filename, bumpfile) {
    this.object = new THREE.Object3D();
    this.object.position.set(x, y, z);
    this.geometry = new THREE.CubeGeometry(width, width, width, 1, 1, 1);
    // atribuir textura a cada face do cubo
    this.computeUvs();
    let texture = new THREE.TextureLoader().load(filename);
    let bumpMap = new THREE.TextureLoader().load(bumpfile);

    let bmaterial = new THREE.MeshBasicMaterial({
      map: texture,
    });

    let pmaterial = new THREE.MeshPhongMaterial({
      map: texture,
      bumpMap: bumpMap,
      specular: 0x121212,
      emissive: 0x000000,
      shininess: 3
    });

    this.mesh = new SceneMesh(bmaterial, pmaterial, this.geometry);
    this.mesh.position.set(0, 0, 0);
    this.object.add(this.mesh);
    scene.add(this.object);

  }

  computeUvs() {
    var face1 = [
      new THREE.Vector2(0, 2 / 3),
      new THREE.Vector2(1 / 4, 2 / 3),
      new THREE.Vector2(0, 1 / 3),
      new THREE.Vector2(1 / 4, 1 / 3)
    ];
    var face2 = [
      new THREE.Vector2(1 / 4, 1),
      new THREE.Vector2(2 / 4, 1),
      new THREE.Vector2(1 / 4, 2 / 3),
      new THREE.Vector2(2 / 4, 2 / 3)
    ];

    var face3 = [
      new THREE.Vector2(1 / 4, 2 / 3),
      new THREE.Vector2(2 / 4, 2 / 3),
      new THREE.Vector2(1 / 4, 1 / 3),
      new THREE.Vector2(2 / 4, 1 / 3)
    ];

    var face4 = [
      new THREE.Vector2(1 / 4, 1 / 3),
      new THREE.Vector2(2 / 4, 1 / 3),
      new THREE.Vector2(1 / 4, 0),
      new THREE.Vector2(2 / 4, 0)
    ];

    var face5 = [
      new THREE.Vector2(2 / 4, 2 / 3),
      new THREE.Vector2(3 / 4, 2 / 3),
      new THREE.Vector2(2 / 4, 1 / 3),
      new THREE.Vector2(3 / 4, 1 / 3)
    ];

    var face6 = [
      new THREE.Vector2(3 / 4, 2 / 3),
      new THREE.Vector2(1, 2 / 3),
      new THREE.Vector2(3 / 4, 1 / 3),
      new THREE.Vector2(1, 1 / 3)
    ];

    this.geometry.faceVertexUvs[0][0] = [face1[0], face1[1], face1[2]];
    this.geometry.faceVertexUvs[0][1] = [face1[2], face1[3], face1[1]];

    this.geometry.faceVertexUvs[0][2] = [face2[0], face2[1], face2[2]];
    this.geometry.faceVertexUvs[0][3] = [face2[2], face2[3], face2[1]];

    this.geometry.faceVertexUvs[0][4] = [face3[0], face3[1], face3[2]];
    this.geometry.faceVertexUvs[0][5] = [face3[2], face3[3], face3[1]];

    this.geometry.faceVertexUvs[0][6] = [face4[0], face4[1], face4[2]];
    this.geometry.faceVertexUvs[0][7] = [face4[2], face4[3], face4[1]];

    this.geometry.faceVertexUvs[0][8] = [face5[0], face5[1], face5[2]];
    this.geometry.faceVertexUvs[0][9] = [face5[2], face5[3], face5[1]];

    this.geometry.faceVertexUvs[0][10] = [face6[0], face6[1], face6[2]];
    this.geometry.faceVertexUvs[0][11] = [face6[2], face6[3], face6[1]];
  }
}


class PoolBall {
  constructor(radius, x, y, z, filename) {
    'use strict';
    this.object = new THREE.Object3D();
    this.object.position.set(x, y, z);
    this.geometry = new THREE.SphereGeometry(radius, 15, 15);
    let texture = new THREE.TextureLoader().load(filename);
    let bmaterial = new THREE.MeshBasicMaterial({
      map: texture
    });
    let pmaterial = new THREE.MeshPhongMaterial({
      specular: 0x999999,
      emissive: 0x000000,
      shininess: 30,
      map: texture
    });

    this.mesh = new SceneMesh(bmaterial, pmaterial, this.geometry);
    this.mesh.position.set(0, 0, 0);

    this.object.add(this.mesh);
    scene.add(this.object);

    this.omega = 0;
    this.alpha = 0;
    this.rads = 0;



  }

  calculatePos(time) {
    let vec = this.object.position.clone();
    this.object.position.x = 10 * (Math.sin(this.rads + time * this.omega));
    this.object.position.z = 10 * (Math.cos(this.rads + time * this.omega));
    this.rads += time * this.omega;
    // para rads nunca dar overflow
    this.rads = this.rads % (2 * Math.PI);

    this.object.rotateOnWorldAxis(AUXVEC, this.omega * time);
    this.object.rotateOnWorldAxis(vec.sub(this.object.position).cross(AUXVEC).normalize(), this.omega * time * 3);
  }

  updateVelocity(time) {
    this.omega += this.alpha * time;
    this.omega = Math.min(this.omega, MAXOMEGA);
    this.omega = Math.max(this.omega, 0);
  }

}

function createLights() {
  directionalLight = new THREE.DirectionalLight(0xFFFFFF, 0.7);
  directionalLight.position.set(20, 20, 20);
  scene.add(directionalLight);

  spotLight = new THREE.PointLight(0x0000FF, 2, 100);
  spotLight.position.set(5, 5, 5);
  scene.add(spotLight);
}


class SceneMesh extends THREE.Mesh {

  constructor(basicMaterial, phongMaterial, geometry) {
    'use strict';
    super(geometry, basicMaterial);
    this.basicMaterial = basicMaterial;
    this.phongMaterial = phongMaterial;
  }

  changeMaterial() {
    this.material = this.material.type == 'MeshPhongMaterial' ? this.basicMaterial : this.phongMaterial;
  }

  togleWireframes() {
    this.basicMaterial.wireframe = this.basicMaterial.wireframe ? false : true;
    this.phongMaterial.wireframe = this.phongMaterial.wireframe ? false : true;
  }

}

class SceneMultiMesh extends SceneMesh {

  constructor(basicMaterials, phongMaterials, geometry) {
    'use strict';
    super(basicMaterials, phongMaterials, geometry);
  }

  changeMaterial() {
    this.material = this.material[0].type == 'MeshPhongMaterial' ? this.basicMaterial : this.phongMaterial;
  }

  togleWireframes() {
    for (let i = 0; i < this.material.length; i++) {
      this.basicMaterial[i].wireframe = this.basicMaterial[i].wireframe ? false : true;
      this.phongMaterial[i].wireframe = this.phongMaterial[i].wireframe ? false : true;
    }
  }

}


function reset() {

  createScene();

  meshes.push(new Board(40, 4, 40, 0, 0, 0, 'textures/chessTexture.jpg').mesh);
  meshes.push(new RubicksCube(6, 0, 5, 0, 'textures/rubicksCubeTexture.png', 'textures/bumpMap.png').mesh);
  ball = new PoolBall(3, 10, 5, 10, 'textures/poolBallTexture.jpeg');
  meshes.push(ball.mesh);

  createLights();
  createCamera();
  createPauseScreen();

  controls = new THREE.OrbitControls(camera);

  controls.autoRotate = true;
  controls.autoRotateSpeed = 0.5;

  paused = false;

  timeStamp = Date.now();
}
